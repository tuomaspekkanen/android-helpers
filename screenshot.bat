@echo off
set ip={YOUR_IP_HERE}
set adb="%ProgramFiles(x86)%\Android\android-sdk\platform-tools\adb.exe"

echo Taking screenshots..

%adb% shell screencap /sdcard/screen.png

%adb% pull /sdcard/screen.png
%adb% shell rm -f /sdcard/screen.png