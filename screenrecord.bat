REM https://video.stackexchange.com/questions/20495/how-do-i-set-up-and-use-ffmpeg-in-windows

@echo off
set ip={YOUR_IP_HERE}
set adb="%ProgramFiles(x86)%\Android\android-sdk\platform-tools\adb.exe"

echo Taking screenrecord..

%adb% shell screenrecord --o raw-frames /sdcard/video.raw

pause

%adb% pull /sdcard/video.raw
%adb% shell rm -f /sdcard/video.raw

echo Video pulled. Converting to mp4..

ffmpeg -f rawvideo -vcodec rawvideo -s {DEVICE_RESOLUTION_HERE} -pix_fmt rgb24 -r 10 -i video.raw  -an -c:v libx264 -pix_fmt yuv420p video.mp4

echo Deleting temporary files..

DEL video.raw