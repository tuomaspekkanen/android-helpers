@echo off
set ip={YOUR_IP_HERE}
set adb="%ProgramFiles(x86)%\Android\android-sdk\platform-tools\adb.exe"

echo This script will establish a connection to your watch on the same WiFi network.
echo Your IP is %ip%

%adb% connect %ip%

timeout /t 10 >nul